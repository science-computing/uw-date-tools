UW Date Tools
=============

Collection of small utilities for dealing with dates and terms at the University of Waterloo in python.


Terms
-------
*uw_date_tools.terms*

### terms.Term [class]

Term is callable with or without a single argument which can be:

* An integer representing a term (i.e 1159, 1165, 1171)
* A string (i.e "Fall 2015", "Spring 2016", "Winter 2017")
* A Date Object from datetime.date

The resulting object can be cast as an int, or a string.

**Properties:**

* **start** a datetime.date for the first day of the first month of a given term.
* **end** a datetime.date for the last day of the last month of the term.
* **next** returns the next chronological term
* **prev** returns the previous chronological term

**Examples:**

    t = Term(1159) # get current term
    t.next
    # > Winter 2016

**Ranges:**

Given a term, it is possible to calculate a range to another term (or parsable int, str, or date).

**Examples:**

    t = Term(1159)
    [int(x) for x in t.range(1179)]
    # > [1159, 1161, 1165, 1169, 1171, 1175, 1179]

The ranges with **always** be in chronological order, no matter the order of input.
