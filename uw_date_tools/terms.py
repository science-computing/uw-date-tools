from datetime import date, datetime, timedelta
from calendar import monthrange


class Term():
    def __init__(self, initial=None):
        self.start = None
        self.end = None
        self.termStr = None
        self.termInt = None
        if initial is None:
            initial = date.today()
        i_type = type(initial)
        if i_type in [str]:
            initial = str(initial)
            if len(initial) == 4:
                self._parseNumeric(initial)
            else:
                self._parseString(initial)
        elif i_type in [int, float]:
            self._parseNumeric(initial)
        elif i_type in [datetime, date]:
            self._parseDate(initial)
        else:
            raise Exception(
                "Can't parse {} into term object".format(
                    i_type
                )
            )

    def _parseNumeric(self, val):
        val = str(val)
        if len(val) != 4:
            raise Exception(
                "Can only parse 4-digit numbers into Term!"
            )
        year = 1990 + 10*int(val[0]) + int(val[1:3])
        self.termInt = self._roundMonth(int(val[3]))
        self.start = date(
            year,
            self.termInt,
            1
        )
        self._calc_end()
        self._calc_termStr()

    def _parseString(self, val):
        comp = val.split(' ')
        if len(comp) < 2:
            raise Exception(
                "'{}' is not a valid term string. Valid: Winter 2015, fall 10, s 2015"
                .format(val)
            )
        term = comp[0].lower()
        if term in ['winter', 'win', 'w']:
            self.termStr = "Winter"
        elif term in ['spring', 'spr', 's']:
            self.termStr = "Spring"
        elif term in ['fall', 'fal', 'f']:
            self.termStr = "Fall"
        self._calc_termInt()

        year = int(comp[1].strip("'"))
        if year < 100:
            if year > 80:
                # assume 99
                year = 1900 + year
            else:
                year = 2000 + year
        elif year > 999 and year < 9999:
            year = year
        else:
            raise Exception("Invalid Year")

        self.start = date(year, self.termInt, 1)
        self._calc_end()

    def _parseDate(self, val):
        self.termInt = self._roundMonth(val.month)
        self.start = date(val.year, self.termInt, 1)
        self._calc_end()
        self._calc_termStr()

    def _roundMonth(self, month):
        month = int(month)
        if month >= 9:
            return 9
        if month >= 5:
            return 5
        if month >= 1:
            return 1

    def _calc_end(self):
        end_month = self.start.month + 3
        end_day = monthrange(self.start.year, end_month)[1]
        self.end = date(self.start.year, end_month, end_day)

    def _calc_termStr(self):
        if self.termInt is 1:
            self.termStr = "Winter"
        elif self.termInt is 5:
            self.termStr = "Spring"
        elif self.termInt is 9:
            self.termStr = "Fall"
        else:
            raise Exception(
                "Cannot parse term string from '{}'"
                .format(self.termInt)
            )

    def _calc_termInt(self):
        if self.termStr is "Winter":
            self.termInt = 1
        elif self.termStr is "Spring":
            self.termInt = 5
        elif self.termStr is "Fall":
            self.termInt = 9
        else:
            raise Exception("Cannot parse term integer (invalid term string)")

    # TYPE CONVERSIONS
    def __int__(self):
        year = int(self.start.year)
        prefix = 1 if year >= 2000 else 0
        return int('{}{}{}'.format(
            prefix,
            str(year)[2:4],
            self.termInt
        ))

    def __str__(self):
        return '{} {}'.format(self.termStr, self.start.year)

    # Relative Operations
    def range(self, target):
        if not isinstance(target, Term):
            target = Term(target)
        r = []
        current = Term(int(self))
        if target.start > self.start:
            while current.start <= target.start:
                r.append(current)
                current = current.next
        else:
            while current.start >= target.start:
                r.insert(0, current)
                current = current.prev
        return r

    # PROPERTIES
    @property
    def next(self):
        # Add 10 days and calculate...
        return Term(self.end + timedelta(days=10))

    @property
    def prev(self):
        return Term(self.start - timedelta(days=10))


def getTermRange(start, end):
    pass
