# Tests for django (if we're in django)
try:
    from django.test import TestCase as DTestCase
    is_django = True
except:
    is_django = False

import unittest
from datetime import date

try:
    from .terms import Term
except:
    from terms import Term


class CreateTermsTestCase(unittest.TestCase):
    def test_from_int(self):
        self.assertEqual(
            int(Term(1151)), 1151
        )
        self.assertEqual(
            int(Term(1159)), 1159
        )
        self.assertEqual(
            int(Term(1155)), 1155
        )
        self.assertEqual(
            str(Term(1151)), 'Winter 2015'
        )
        self.assertEqual(
            str(Term(1159)), 'Fall 2015'
        )
        self.assertEqual(
            str(Term(1155)), 'Spring 2015'
        )
        with self.assertRaises(Exception):
            Term(0000)
        with self.assertRaises(Exception):
            Term(11511)

    def test_from_string(self):
        self.assertEqual(
            int(Term('1151')), 1151
        )
        self.assertEqual(
            int(Term('1157')), 1155
        )
        self.assertEqual(
            int(Term('Spring 2015')), 1155
        )
        self.assertEqual(
            int(Term('Spr 2019')), 1195
        )
        self.assertEqual(
            int(Term('win 19')), 1191
        )
        with self.assertRaises(Exception):
            Term('rer 13423')
        with self.assertRaises(Exception):
            Term('roirorfv')

    def test_from_date(self):
        self.assertEqual(
            int(Term(date(2012, 1, 10))), 1121
        )
        self.assertEqual(
            int(Term(date(2012, 7, 5))), 1125
        )
        self.assertEqual(
            int(Term(date(2012, 10, 1))), 1129
        )


class RelativeTermsTestCase(unittest.TestCase):

    def test_next(self):
        self.assertEqual(
            int(Term(1151).next), 1155
        )
        self.assertEqual(
            int(Term(1155).next), 1159
        )
        self.assertEqual(
            int(Term(1159).next), 1161
        )

    def test_prev(self):
        self.assertEqual(
            int(Term(1151).prev), 1149
        )
        self.assertEqual(
            int(Term(1155).prev), 1151
        )
        self.assertEqual(
            int(Term(1159).prev), 1155
        )

    def test_range(self):
        self.assertEqual(
            [int(x) for x in Term(1159).range(1179)],
            [1159, 1161, 1165, 1169, 1171, 1175, 1179]
        )
        self.assertEqual(
            [int(x) for x in Term(1179).range(1159)],
            [1159, 1161, 1165, 1169, 1171, 1175, 1179]
        )
        self.assertEqual(
            [int(x) for x in Term(1179).range(1179)],
            [1179]
        )


if __name__ == '__main__':
    unittest.main()
