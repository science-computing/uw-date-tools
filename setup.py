import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='uw-date-tools',
    version='1.0.0',
    packages=find_packages(),
    include_package_data=True,
    license='Unlicense',
    description='Basic tools for handling University of Waterloo dates in python',
    long_description=README,
    url='https://git.uwaterloo.ca/science-computing/uw-date-tools',
    author='Mirko Vucicevich',
    author_email='mvucicev@uwaterloo.ca',
    classifiers=[
        'Intended Audience :: Developers',
        'License :: Unlicense',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
)
